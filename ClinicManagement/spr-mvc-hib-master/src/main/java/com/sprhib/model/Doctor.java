package com.sprhib.model;

public class Doctor extends User{

	public Doctor(String username, String password, String fullName, int startHour, int finishHour) {
		super(username, password, 3);
		this.setFullName(fullName);
		this.setStartHour(startHour);
		this.setFinishHour(finishHour);
	}

}

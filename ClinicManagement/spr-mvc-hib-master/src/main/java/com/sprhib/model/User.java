package com.sprhib.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User {

	@Id
	@GeneratedValue
	private Integer id;
	
	private String username;
	
	private String password;
	
	@Column(name = "typeID")
	private int typeID;
	
	@Column(name = "fullname")
	private String fullName;
	
	@Column(name = "starthour")
	private Integer startHour;
	
	@Column(name = "finishhour")
	private Integer finishHour;
	
	public User(String username, String password, int typeID) {
		super();
		this.username = username;
		this.password = password;
		this.typeID = typeID;
	}
	public User() {
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getTypeID() {
		return typeID;
	}
	public void setTypeID(int typeID) {
		this.typeID = typeID;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public int getStartHour() {
		return startHour;
	}
	public void setStartHour(int startHour) {
		this.startHour = startHour;
	}
	public int getFinishHour() {
		return finishHour;
	}
	public void setFinishHour(int finishHour) {
		this.finishHour = finishHour;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", typeID=" + typeID + ", fullName=" + fullName + ", startHour=" + startHour
				+ ", finishHour=" + finishHour + "]";
	}

	
}

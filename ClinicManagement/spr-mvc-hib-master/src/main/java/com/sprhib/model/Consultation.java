package com.sprhib.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "consultation")
public class Consultation {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	//@ManyToOne
	@JoinColumn(name="doctorId")
	private int doctorId;
	
	@JoinColumn(name="patientId")
	private Integer patientId;
	
	private Integer startHour;
	private Integer finishHour;
	private Date consultationDate;
	
	public Consultation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Consultation(int doctorId, Integer patientId, Integer startHour, Integer finishHour, Date consultationDate) {
		super();
		this.doctorId = doctorId;
		this.patientId = patientId;
		this.startHour = startHour;
		this.finishHour = finishHour;
		this.consultationDate = consultationDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public Integer getStartHour() {
		return startHour;
	}

	public void setStartHour(Integer startHour) {
		this.startHour = startHour;
	}

	public Integer getFinishHour() {
		return finishHour;
	}

	public void setFinishHour(Integer finishHour) {
		this.finishHour = finishHour;
	}

	public Date getConsultationDate() {
		return consultationDate;
	}

	public void setConsultationDate(Date consultationDate) {
		this.consultationDate = consultationDate;
	}

	public Integer getPatientId() {
		return patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	@Override
	public String toString() {
		return "Consultation [id=" + id + ", doctorId=" + doctorId + ", patientId=" + patientId + ", startHour="
				+ startHour + ", finishHour=" + finishHour + ", consultationDate=" + consultationDate + "]";
	}
	
	

}

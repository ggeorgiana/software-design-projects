package com.sprhib.dao;

import java.util.List;

import com.sprhib.model.Patient;

public interface PatientDAO {
	public void addPatient(Patient patient);
	public void updatePatient(Patient patient);
	public Patient getPatient(int id);
	public void deletePatient(int id);
	public List<Patient> getPatients();
}

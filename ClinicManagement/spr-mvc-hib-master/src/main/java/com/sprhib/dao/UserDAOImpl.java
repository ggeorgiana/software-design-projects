package com.sprhib.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprhib.model.User;

@Repository
public class UserDAOImpl implements UserDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public void addUser(User user) {
		getCurrentSession().save(user);	
	}

	public void updateUser(User user) {
		User userToUpdate = getUser(user.getId());
		userToUpdate.setUsername(user.getUsername());
		userToUpdate.setPassword(user.getPassword());
		getCurrentSession().update(userToUpdate);
	}

	public User getUser(int id) {
		User user = (User) getCurrentSession().get(User.class,id);
		return user;
	}

	public void deleteUser(int id) {
		User user = getUser(id);
		if(user != null)
			getCurrentSession().delete(user);
	}

	@SuppressWarnings("unchecked")
	public List<User> getUsers() {
		return getCurrentSession().createQuery("from User" ).list();
	}
	
	public List<User> getDoctors() {
		List<User> allUsers = getUsers();
		List<User> result = new ArrayList<User>();
		for(User user: allUsers){
			System.out.println("ID useri: \n"+user.getTypeID());
			if(user.getTypeID() == 3){
				result.add(user);
				System.out.println("s-a gasit doctor cu ID: "+user.getTypeID());
				System.out.println("Doctor gasit: "+result.toString());
			}	
			else{
				System.out.println("Nu exista doctori inregistrati!");
			}
		}
		return result;
	}

}

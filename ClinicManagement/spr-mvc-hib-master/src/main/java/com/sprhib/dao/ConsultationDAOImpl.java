package com.sprhib.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprhib.model.Consultation;

@Repository
public class ConsultationDAOImpl implements ConsultationDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void addConsultation(Consultation consultation) {
		getCurrentSession().save(consultation);
	}

	@Override
	public void updateConsultation(Consultation consultation) {
		Consultation consultationToUpdate = getConsultation(consultation.getId());
		//consultationToUpdate.setDoctorId(consultation.getDoctorId());
		consultationToUpdate.setConsultationDate(consultation.getConsultationDate());
		consultationToUpdate.setStartHour(consultation.getStartHour());
		consultationToUpdate.setFinishHour(consultation.getFinishHour());
		getCurrentSession().update(consultationToUpdate);
	}

	@Override
	public Consultation getConsultation(int id) {
		Consultation consultation = (Consultation) getCurrentSession().get(Consultation.class, id);
		return consultation;
	}

	@Override
	public void deleteConsultation(int id) {
		Consultation consultation = getConsultation(id);
		if (consultation != null)
			getCurrentSession().delete(consultation);
	}

	@SuppressWarnings("unchecked")
	public List<Consultation> getConsultations() {
		return getCurrentSession().createQuery("from Consultation").list();
	}

	public List<Consultation> getConsultationByDate(Date date, int doctorId) {
		List<Consultation> allConsultations = getConsultations();
		List<Consultation> result = new ArrayList<Consultation>();
		for (Consultation consultation : allConsultations) {
			if (consultation.getConsultationDate().compareTo(date) == 0 && consultation.getDoctorId() == doctorId){
				result.add(consultation);
				System.out.println("Consultatii ziua resp DAO: "+consultation.toString());
			}
		}
		return result;
	}

}

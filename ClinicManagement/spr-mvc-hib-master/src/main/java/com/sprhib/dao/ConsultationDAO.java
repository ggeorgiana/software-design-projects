package com.sprhib.dao;

import java.util.Date;
import java.util.List;

import com.sprhib.model.Consultation;

public interface ConsultationDAO {
	public void addConsultation(Consultation consultation);
	public void updateConsultation(Consultation consultation);
	public Consultation getConsultation(int id);
	public void deleteConsultation(int id);
	public List<Consultation> getConsultations();
	public List<Consultation> getConsultationByDate(Date date, int doctorId) ;

}

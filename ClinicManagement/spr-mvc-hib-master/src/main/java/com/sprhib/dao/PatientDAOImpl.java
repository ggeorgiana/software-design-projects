package com.sprhib.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprhib.model.Patient;

@Repository
public class PatientDAOImpl implements PatientDAO{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void addPatient(Patient patient) {
		getCurrentSession().save(patient);
	}

	@Override
	public void updatePatient(Patient patient) {
		Patient patientToUpdate = getPatient(patient.getId());
		patientToUpdate.setName(patient.getName());
		patientToUpdate.setIdCardNo(patient.getIdCardNo());
		patientToUpdate.setPersonalCode(patient.getPersonalCode());
		patientToUpdate.setDateOfBirth(patient.getDateOfBirth());
		patientToUpdate.setAddress(patient.getAddress());
		getCurrentSession().update(patientToUpdate);		
	}

	@Override
	public Patient getPatient(int id) {
		Patient patient = (Patient) getCurrentSession().get(Patient.class,id);
		return patient;
	}

	@Override
	public void deletePatient(int id) {
		Patient patient = getPatient(id);
		if(patient != null)
			getCurrentSession().delete(patient);		
	}

	@SuppressWarnings("unchecked")
	public List<Patient> getPatients() {
		return getCurrentSession().createQuery("from Patient" ).list();
	}

}

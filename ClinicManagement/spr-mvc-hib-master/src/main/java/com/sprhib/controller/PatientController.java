package com.sprhib.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sprhib.model.Patient;
import com.sprhib.service.PatientService;

@Controller
@RequestMapping(value = "/patient")
public class PatientController {
	
	@Autowired
	private PatientService patientService;
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView addPatientPage() {
		ModelAndView modelAndView = new ModelAndView("add-patient-form");
		modelAndView.addObject("patient", new Patient());
		return modelAndView;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addingPatient(@ModelAttribute Patient patient) {

		ModelAndView modelAndView = new ModelAndView("secretary-form");
		patientService.addPatient(patient);

		String message = "A patient was successfully added.";
		modelAndView.addObject("message", message);

		return modelAndView;
	}

	@RequestMapping(value = "/list")
	public ModelAndView listOfPatients() {
		ModelAndView modelAndView = new ModelAndView("list-of-patients");

		List<Patient> patients = patientService.getPatients();
		modelAndView.addObject("patients", patients);

		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editPatientPage(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("edit-patient-form");
		Patient patient = patientService.getPatient(id);
		modelAndView.addObject("patient", patient);
		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public ModelAndView edditingPatient(@ModelAttribute Patient patient, @PathVariable Integer id) {

		ModelAndView modelAndView = new ModelAndView("doctor-form");

		patientService.updatePatient(patient);

		String message = "Patient was successfully edited.";
		modelAndView.addObject("message", message);

		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deletePatient(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("doctor-form");
		patientService.deletePatient(id);
		String message = "Patient was successfully deleted.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}

}

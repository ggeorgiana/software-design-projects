package com.sprhib.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sprhib.model.Consultation;
import com.sprhib.model.ConsultationDTO;
import com.sprhib.model.Patient;
import com.sprhib.model.User;
import com.sprhib.service.ConsultationService;
import com.sprhib.service.PatientService;
import com.sprhib.service.UserService;

@Controller
@RequestMapping(value = "/consultation")
public class ConsultationController {

	@Autowired
	private ConsultationService consultationService;

	@Autowired
	private PatientService patientService;

	@Autowired
	private UserService userService;
	private Integer doctorId;

	@RequestMapping(value = "/add/{id}", method = RequestMethod.GET)
	public ModelAndView addConsultationPage(@PathVariable Integer id) {
		doctorId = id;
		System.out.println("doctor id:" + id);
		ModelAndView modelAndView = new ModelAndView("add-consultation-form");
		// modelAndView.addObject("account", new
		// Account(clientService.getClient(id), null, null, 0));
		modelAndView.addObject("consultation", new Consultation(id, 0, 0, 0, null));
		return modelAndView;
	}

	@RequestMapping(value = "/addConsultation/{id}", method = RequestMethod.POST)
	public ModelAndView addingConsultation(@ModelAttribute ConsultationDTO consultationDto, @PathVariable Integer id) {
		String message = null;

		System.out.println("doctor id:" + doctorId);
		System.out.println(consultationDto.getPatientId());
		System.out.println(consultationDto.getStartHour());
		System.out.println(consultationDto.getFinishHour());
		System.out.println(consultationDto.getConsultationDate());

		User doctor = userService.getUser(doctorId);
		Patient patient = patientService.getPatient(consultationDto.getPatientId());
		ModelAndView modelAndView = new ModelAndView("secretary-form");

		DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
		Date consultationDate = null;
		try {
			consultationDate = formatter.parse(consultationDto.getConsultationDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(consultationDate);
		Consultation consultation = null;
		System.out.println(consultationDto.getStartHour() + " < " + consultationDto.getFinishHour());
		System.out.println(consultationDto.getStartHour() + " > " + doctor.getStartHour());
		System.out.println(consultationDto.getFinishHour() + " < " + doctor.getFinishHour());

		if (consultationDto.getStartHour() < consultationDto.getFinishHour()
				&& consultationDto.getStartHour() >= doctor.getStartHour()
				&& consultationDto.getFinishHour() <= doctor.getFinishHour()) {
			System.out.println(consultationService.getConsultationByDate(consultationDate, doctorId).toString());
			List<Consultation> consultationsByDate = consultationService.getConsultationByDate(consultationDate,
					doctorId);
			// nu mai are consultatii in ziua resp
			if (consultationsByDate.isEmpty()) {
				consultation = new Consultation(doctorId, consultationDto.getPatientId(),
						consultationDto.getStartHour(), consultationDto.getFinishHour(), consultationDate);
				consultationService.addConsultation(consultation);
				message = "S-a adaugat o consultatie la doctorul " + doctor.getFullName() + " pacientului: "
						+ patient.getName();
			} else {
				for (Consultation con : consultationsByDate) {
					System.out.println(con.getFinishHour() + " < " + consultationDto.getStartHour());
					System.out.println(con.getStartHour() + " > " + consultationDto.getFinishHour());
					// daca nu se suprapun consultatiile
					if (con.getFinishHour() <= consultationDto.getStartHour()
							|| con.getStartHour() >= consultationDto.getFinishHour()) {
						consultation = new Consultation(doctorId, consultationDto.getPatientId(),
								consultationDto.getStartHour(), consultationDto.getFinishHour(), consultationDate);
						consultationService.addConsultation(consultation);
						message = "S-a adaugat o consultatie la doctorul " + doctor.getFullName() + " pacientului: "
								+ patient.getName();
					} else {
						consultation = null;
						message = "Doctorul " + doctor.getFullName() + " are o alta consultatie la ora respectiva!";
						System.out.println(
								"Doctorul " + doctor.getFullName() + " are o alta consultatie la ora respectiva!");
						break;
						
					}
				}
			}
		}
		// nu are consultatii in ziua resp
		else {
			message = "Orele introduse sunt gresite sau Doctorul " + doctor.getFullName() + " este indisponibil!";
			System.out.println("Orele introduse sunt gresite sau Doctor indisponibil!\n");
			// clientService.getClientAccounts(clientId).add(acc);
		}
		modelAndView.addObject("message", message);

		return modelAndView;
	}

	@RequestMapping(value = "/list/{id}")
	public ModelAndView listOfConsultations(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("list-of-consultations");

		List<Consultation> consultations = consultationService.getConsultations();
		List<Consultation> patientConsultations = new ArrayList<Consultation>();
		for (Consultation con : consultations) {
			if (con.getPatientId() == id) {
				patientConsultations.add(con);
				System.out.println("consultatii ale pacientului " + id + ": " + con.toString());
			}
		}
		modelAndView.addObject("consultations", patientConsultations);

		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editConsultationPage(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("edit-consultation-form");
		System.out.println("THIS IS ID: "+id);
		Consultation consultation = consultationService.getConsultation(id);
		System.out.println("DADA  "+consultation.toString());
		modelAndView.addObject("consultation",consultation);
		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public ModelAndView edditingConsultation(@ModelAttribute Consultation consultation,
			@PathVariable Integer id) {
		System.out.println("ID:"+id);
		ModelAndView modelAndView = new ModelAndView("secretary-form");

		consultationService.deleteConsultation(id);
		consultationService.addConsultation(consultation);

		String message = "Consultation was successfully edited.";
		modelAndView.addObject("message", message);

		return modelAndView;
	}
	@RequestMapping(value = "/checkin/{id}", method = RequestMethod.GET)
	public ModelAndView checkinConsultationPage(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("doctor-form");
		Consultation consultation = consultationService.getConsultation(id);
		Patient patient = patientService.getPatient(consultation.getPatientId());
		User doctor = userService.getUser(consultation.getDoctorId());
		String message = "Pacientul "+patient.getName()+ " a sosit la consultatia din data de "+consultation.getConsultationDate()+", de la ora "+consultation.getStartHour()+":00, la doctorul "+doctor.getFullName();
		modelAndView.addObject("message", message);
		return modelAndView;
	}


	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteConsultation(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("secretary-form");
		consultationService.deleteConsultation(id);
		String message = "Consultation was successfully deleted.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}

}

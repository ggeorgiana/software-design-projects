package com.sprhib.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sprhib.dao.ConsultationDAO;
import com.sprhib.model.Consultation;

@Service
@Transactional
public class ConsultationServiceImpl implements ConsultationService {
	
	@Autowired
	private ConsultationDAO consultationDAO;

	@Override
	public void addConsultation(Consultation consultation) {
		consultationDAO.addConsultation(consultation);
	}

	@Override
	public void updateConsultation(Consultation consultation) {
		consultationDAO.updateConsultation(consultation);		
	}

	@Override
	public Consultation getConsultation(int id) {
		return consultationDAO.getConsultation(id);
	}

	@Override
	public void deleteConsultation(int id) {
		consultationDAO.deleteConsultation(id);	
	}

	@Override
	public List<Consultation> getConsultations() {
		return consultationDAO.getConsultations();
	}

	@Override
	public List<Consultation> getConsultationByDate(Date date, int doctorId) {
		return consultationDAO.getConsultationByDate(date, doctorId);
	}

}

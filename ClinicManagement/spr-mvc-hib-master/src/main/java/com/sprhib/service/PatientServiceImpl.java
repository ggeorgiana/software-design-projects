package com.sprhib.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sprhib.dao.PatientDAO;
import com.sprhib.model.Patient;

@Service
@Transactional
public class PatientServiceImpl implements PatientService{
	
	@Autowired
	private PatientDAO patientDAO;

	@Override
	public void addPatient(Patient patient) {
		patientDAO.addPatient(patient);		
	}

	@Override
	public void updatePatient(Patient patient) {
		patientDAO.updatePatient(patient);		
	}

	@Override
	public Patient getPatient(int id) {
		return patientDAO.getPatient(id);
	}

	@Override
	public void deletePatient(int id) {
		patientDAO.deletePatient(id);		
	}

	@Override
	public List<Patient> getPatients() {
		return patientDAO.getPatients();
	}

}

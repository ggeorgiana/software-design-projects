<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>List of consultations</title>
</head>
<body>
	<h1>List of consultations</h1>
	<p>Here you can see the list of the consultations, edit them, remove or
		update.</p>
	<table border="1px" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th width="10%">id</th>
				<th width="15%">Doctor ID</th>
				<th width="10%">Consultation Date</th>
				<th width="10%">Start Hour</th>
				<th width="10%">Finish Hour</th>
				<th width="10%">actions</th>

			</tr>
		</thead>
		<tbody>
			<c:forEach var="consultation" items="${consultations}">
				<tr>
					<td>${consultation.id}</td>
					<td>${consultation.doctorId}</td>
					<td>${consultation.consultationDate}</td>
					<td>${consultation.startHour}:00</td>
					<td>${consultation.finishHour}:00</td>
					<td><a href="${pageContext.request.contextPath}/consultation/edit/${consultation.id}.html">Edit</a><br />
						<a href="${pageContext.request.contextPath}/consultation/delete/${consultation.id}.html">Delete</a><br />
						<a href="${pageContext.request.contextPath}/consultation/checkin/${consultation.id}.html">Check in</a><br />				
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<p>
		<a href="${pageContext.request.contextPath}/user/login.html">Administrator page</a>
	</p>

</body>
</html>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Add employee page</title>
</head>
<body>
	<h1>Add user page</h1>
	<p>Here you can add a new user.</p>
	<form:form method="POST" commandName="user"
		action="${pageContext.request.contextPath}/user/add.html">
		<table>
			<tbody>
				<tr>
					<td>Name:</td>
					<td><form:input path="username" /></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td><form:input path="password" /></td>
				</tr>
				<tr>
					<td>User Type:</td>
					<td><select name="typeID">
							<option value="1">Administrator</option>
							<option value="2">Secretary</option>
							<option value="3">Doctor</option>
					</select></td>

				</tr>
				<tr>
					<td><input type="submit" value="Add" /></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</form:form>

	<p>
		<a href="${pageContext.request.contextPath}/user/login.html">Administrator
			page</a>
	</p>
</body>
</html>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Edit consultation page</title>
</head>
<body>
	<h1>Edit consultation page</h1>
	<p>Here you can edit the existing consultation.</p>
	<p>${message}</p>
	<form:form method="POST" commandName="consultation"
		action="${pageContext.request.contextPath}/consultation/edit/${consultation.id}.html">
		<table>
			<tbody>
				<tr>
					<td>Consultation Date:</td>
					<td><form:input path="consultationDate" /></td>
				</tr>
				<tr>
					<td>Start Hour:</td>
					<td><form:input path="startHour" /></td>
				</tr>
				<tr>
					<td>Finish Hour:</td>
					<td><form:input path="finishHour" /></td>
				</tr>
				<tr>
					<td><input type="submit" value="Edit" /></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</form:form>

	<p>
		<a href="${pageContext.request.contextPath}/user/login.html">Administrator
			page</a>
	</p>
</body>
</html>
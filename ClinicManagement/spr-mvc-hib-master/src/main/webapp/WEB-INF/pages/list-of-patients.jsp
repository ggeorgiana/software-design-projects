<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>List of patients</title>
</head>
<body>
	<h1>List of patients</h1>
	<p>Here you can see the list of the patients, edit them, remove or
		update.</p>
	<table border="1px" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th width="10%">id</th>
				<th width="15%">name</th>
				<th width="10%">idCardNo</th>
				<th width="10%">personalCode</th>
				<th width="10%">dateOfBirth</th>
				<th width="10%">address</th>
				<th width="10%">actions</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="patient" items="${patients}">
				<tr>
					<td>${patient.id}</td>
					<td>${patient.name}</td>
					<td>${patient.idCardNo}</td>
					<td>${patient.personalCode}</td>
					<td>${patient.dateOfBirth}</td>
					<td>${patient.address}</td>
					<td><a href="${pageContext.request.contextPath}/patient/edit/${patient.id}.html">Edit</a><br />
						<a href="${pageContext.request.contextPath}/patient/delete/${patient.id}.html">Delete</a><br />				
						<a href="${pageContext.request.contextPath}/consultation/list/${patient.id}.html">Consultations </a><br />
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<p>
		<a href="${pageContext.request.contextPath}/user/login.html">Administrator page</a>
	</p>

</body>
</html>
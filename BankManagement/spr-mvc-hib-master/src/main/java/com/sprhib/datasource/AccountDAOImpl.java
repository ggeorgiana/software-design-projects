package com.sprhib.datasource;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprhib.domain.Account;

@Repository
public class AccountDAOImpl implements AccountDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private Session getCurrentSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public void addAccount(Account account) {
		getCurrentSession().save(account);
		
	}

	@Override
	public void updateAccount(Account account) {
		Account accountToUpdate = getAccount(account.getId());
		accountToUpdate.setClient(account.getClient());
		accountToUpdate.setType(account.getType());
		accountToUpdate.setCreationDate(account.getCreationDate());
		accountToUpdate.setBalance(account.getBalance());
		getCurrentSession().update(accountToUpdate);
		
	}

	public Account getAccount(int id) {
		Account account = (Account) getCurrentSession().get(Account.class, id);
		return account;
	}

	public void deleteAccount(int id) {
		Account account = getAccount(id);
		if(account != null)
			getCurrentSession().delete(account);			
	}

	@SuppressWarnings("unchecked")
	public List<Account> getAccounts() {
		return getCurrentSession().createQuery("from Account" ).list();
	}
}


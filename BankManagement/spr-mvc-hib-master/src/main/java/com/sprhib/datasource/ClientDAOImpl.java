package com.sprhib.datasource;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprhib.domain.Client;

@Repository
public class ClientDAOImpl implements ClientDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public void addClient(Client client) {
		getCurrentSession().save(client);
	}

	public void updateClient(Client client) {
		Client clientToUpdate = getClient(client.getId());
		clientToUpdate.setName(client.getName());
		clientToUpdate.setIdCardNo(client.getIdCardNo());
		clientToUpdate.setPersonalCode(client.getPersonalCode());
		clientToUpdate.setAddress(client.getAddress());
		getCurrentSession().update(clientToUpdate);
	}

	public Client getClient(int id) {
		Client client = (Client) getCurrentSession().get(Client.class, id);
		return client;
	}

	public void deleteClient(int id) {
		Client client = getClient(id);
		if(client != null)
			getCurrentSession().delete(client);		
	}

	@SuppressWarnings("unchecked")
	public List<Client> getClients() {
		return getCurrentSession().createQuery("from Client" ).list();
	}
}


package com.sprhib.datasource;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprhib.domain.Account;
import com.sprhib.domain.Transaction;

@Repository
public class TransactionDAOImpl implements TransactionDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession(){
		return sessionFactory.getCurrentSession();
	}

	public void addTransaction(Transaction transaction) {
		getCurrentSession().save(transaction);
	}
	@Override
	public Transaction getTransaction(int id) {
		Transaction transaction = (Transaction) getCurrentSession().get(Account.class, id);
		return transaction;
	}

	@SuppressWarnings("unchecked")
	public List<Transaction> getTransactions() {
		return getCurrentSession().createQuery("from Transaction").list();
	}

}


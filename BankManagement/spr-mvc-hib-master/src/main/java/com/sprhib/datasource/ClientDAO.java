package com.sprhib.datasource;

import java.util.List;

import com.sprhib.domain.Client;

public interface ClientDAO {
	public void addClient(Client client);
	public void updateClient(Client client);
	public Client getClient(int id);
	public void deleteClient(int id);
	public List<Client> getClients();

}

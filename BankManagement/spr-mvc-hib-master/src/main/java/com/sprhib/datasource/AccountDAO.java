package com.sprhib.datasource;

import java.util.List;

import com.sprhib.domain.Account;

public interface AccountDAO {
	public void addAccount(Account account);
	public void updateAccount(Account account);
	public Account getAccount(int id);
	public void deleteAccount(int id);
	public List<Account> getAccounts();
}

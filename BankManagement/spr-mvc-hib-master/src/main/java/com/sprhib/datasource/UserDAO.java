package com.sprhib.datasource;

import java.util.List;

import com.sprhib.domain.User;

public interface UserDAO {
	public void addUser(User user);
	public void updateUser(User user);
	public User getUser(int id);
	public void deleteUser(int id);
	public List<User> getUsers();

}

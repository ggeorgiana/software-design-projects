package com.sprhib.datasource;

import java.util.List;

import com.sprhib.domain.Transaction;

public interface TransactionDAO {
	public void addTransaction(Transaction transaction);
	public Transaction getTransaction(int id);
	public List<Transaction> getTransactions();
}

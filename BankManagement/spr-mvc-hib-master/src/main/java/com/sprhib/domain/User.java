package com.sprhib.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class User {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	private String username;
	
	private String password;
	
	private boolean isAdministrator;
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(String username, String password, boolean isAdministrator) {
		super();
		this.username = username;
		this.password = password;
		this.isAdministrator = isAdministrator;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isAdministrator() {
		return isAdministrator;
	}
	public void setAdministrator(boolean administrator) {
		this.isAdministrator = administrator;
	}
	
}

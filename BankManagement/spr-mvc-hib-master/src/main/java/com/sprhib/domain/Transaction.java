package com.sprhib.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "transactions")
public class Transaction {

	public enum Type {
		DEPOSIT, WITHDRAW, TRANSFER, PAYBILL
	};

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	private int idClient;
	
	@Column(name = "accountId1")
	private int accountId1;
	
	@Column(name = "accountId2")
	private int accountId2;


	@ManyToOne
	@JoinColumn(name = "account")
	private Account account;

	@Column(name = "transactionDate")
	private Date transactionDate;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private Type type;

	@Column(name = "amount")
	private double amount;

	public Transaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Transaction(int idClient, int accountId1, int accountId2, Account account, Date transactionDate, Type type,
			double amount) {
		super();
		this.idClient = idClient;
		this.accountId1 = accountId1;
		this.accountId2 = accountId2;
		this.account = account;
		this.transactionDate = transactionDate;
		this.type = type;
		this.amount = amount;
	}



	public int getAccountId1() {
		return accountId1;
	}

	public void setAccountId1(int accountId1) {
		this.accountId1 = accountId1;
	}

	public int getAccountId2() {
		return accountId2;
	}

	public void setAccountId2(int accountId2) {
		this.accountId2 = accountId2;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
}

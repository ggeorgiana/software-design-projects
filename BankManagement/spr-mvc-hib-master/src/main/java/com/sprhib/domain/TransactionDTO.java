package com.sprhib.domain;

public class TransactionDTO {
	private int accountId1,accountId2;
	private String transactionDate;
	private String type;
	private String amount;
	
	public int getAccountId1() {
		return accountId1;
	}
	public void setAccountId1(int accountId1) {
		this.accountId1 = accountId1;
	}
	public int getAccountId2() {
		return accountId2;
	}
	public void setAccountId2(int accountId2) {
		this.accountId2 = accountId2;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}		
}


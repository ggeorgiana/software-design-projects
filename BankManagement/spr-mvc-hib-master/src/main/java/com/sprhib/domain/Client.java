package com.sprhib.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "clients")
public class Client {

	@Id
	@GeneratedValue
	private Integer id;
	private String name;
	private String idCardNo;
	private String personalCode;
	private String address;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "client")
	private Set<Account> accounts = new HashSet<Account>();
	
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Client(String name, String idCardNo, String personalCode, String address) {
		super();
		this.name = name;
		this.idCardNo = idCardNo;
		this.personalCode = personalCode;
		this.address = address;
	}

	
	public Set<Account> getAccounts() {
		return accounts;
	}


	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdCardNo() {
		return idCardNo;
	}

	public void setIdCardNo(String idCardNo) {
		this.idCardNo = idCardNo;
	}

	public String getPersonalCode() {
		return personalCode;
	}

	public void setPersonalCode(String personalCode) {
		this.personalCode = personalCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}

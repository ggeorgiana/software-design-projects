package com.sprhib.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sprhib.service.TransactionService;
import com.sprhib.datasource.TransactionDAO;
import com.sprhib.domain.Transaction;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	private TransactionDAO transactionDAO;
	
	public void addTransaction(Transaction transaction) {
		transactionDAO.addTransaction(transaction);
	}
	
	public List<Transaction> getTransactions() {
		return transactionDAO.getTransactions();
	}

	public Transaction getTransaction(int id) {
		return transactionDAO.getTransaction(id);
	}
	
}

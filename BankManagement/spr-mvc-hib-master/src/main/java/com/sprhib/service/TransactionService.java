package com.sprhib.service;

import java.util.List;

import com.sprhib.domain.Transaction;

public interface TransactionService {
	public void addTransaction(Transaction transaction);
	public Transaction getTransaction(int id);
	public List<Transaction> getTransactions();
}

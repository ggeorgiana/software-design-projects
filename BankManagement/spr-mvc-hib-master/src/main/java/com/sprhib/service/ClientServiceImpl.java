package com.sprhib.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sprhib.service.ClientService;
import com.sprhib.datasource.ClientDAO;
import com.sprhib.domain.Account;
import com.sprhib.domain.Client;

@Service
@Transactional
public class ClientServiceImpl implements ClientService{

	@Autowired
	private ClientDAO clientDAO;
	
	public void addClient(Client client) {
		clientDAO.addClient(client);
	}

	public void updateClient(Client client) {
		clientDAO.updateClient(client);		
	}

	public Client getClient(int id) {
		return clientDAO.getClient(id);
	}

	public void deleteClient(int id) {
		clientDAO.deleteClient(id);
	}

	public List<Client> getClients() {
		return clientDAO.getClients();
	}

	public Set<Account> getClientAccounts(int id) {
		return clientDAO.getClient(id).getAccounts();
	}

}

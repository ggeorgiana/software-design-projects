package com.sprhib.service;

import java.util.List;

import com.sprhib.domain.User;


public interface UserService {
	public void addUser(User user);
	public void updateUser(User user);
	public User getUser(int id);
	public void deleteUser(int id);
	public List<User> getUsers();
	public User login(String username, String password);
}

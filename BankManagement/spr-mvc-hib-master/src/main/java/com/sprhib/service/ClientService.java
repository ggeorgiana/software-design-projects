package com.sprhib.service;

import java.util.List;
import java.util.Set;

import com.sprhib.domain.Account;
import com.sprhib.domain.Client;

public interface ClientService {
	public void addClient(Client client);
	public void updateClient(Client client);
	public Client getClient(int id);
	public void deleteClient(int id);
	public List<Client> getClients();
	public Set<Account> getClientAccounts(int id);
}

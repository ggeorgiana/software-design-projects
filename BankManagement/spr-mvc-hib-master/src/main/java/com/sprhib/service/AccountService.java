package com.sprhib.service;

import java.util.List;

import com.sprhib.domain.Account;

public interface AccountService {
	
	public void addAccount(Account account);
	public void updateAccount(Account account);
	public Account getAccount(int id);
	public void deleteAccount(int id);
	public List<Account> getAccounts();
}

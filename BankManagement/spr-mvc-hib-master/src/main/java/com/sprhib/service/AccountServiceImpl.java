package com.sprhib.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sprhib.service.AccountService;
import com.sprhib.datasource.AccountDAO;
import com.sprhib.domain.Account;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;

	public void addAccount(Account account) {
		accountDAO.addAccount(account);
	}

	public void updateAccount(Account account) {
		accountDAO.updateAccount(account);
	}

	public Account getAccount(int id) {
		return accountDAO.getAccount(id);
	}

	public void deleteAccount(int id) {
		accountDAO.deleteAccount(id);
	}

	public List<Account> getAccounts() {
		return accountDAO.getAccounts();
	}

}

package com.sprhib.presentation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sprhib.service.ClientService;
import com.sprhib.domain.Client;

@Controller
@RequestMapping(value = "/client")
public class ClientController {
	
	@Autowired
	private ClientService clientService;

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView addClientPage() {
		ModelAndView modelAndView = new ModelAndView("add-client-form");
		modelAndView.addObject("client", new Client());
		return modelAndView;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addingClient(@ModelAttribute Client client) {

		ModelAndView modelAndView = new ModelAndView("employee-form");
		clientService.addClient(client);

		String message = "A client was successfully added.";
		modelAndView.addObject("message", message);

		return modelAndView;
	}

	@RequestMapping(value = "/list")
	public ModelAndView listOfClients() {
		ModelAndView modelAndView = new ModelAndView("list-of-clients");

		List<Client> clients = clientService.getClients();
		modelAndView.addObject("clients", clients);

		return modelAndView;
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editClientPage(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("edit-client-form");
		Client client = clientService.getClient(id);
		modelAndView.addObject("client", client);
		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public ModelAndView edditingClient(@ModelAttribute Client client, @PathVariable Integer id) {

		ModelAndView modelAndView = new ModelAndView("employee-form");

		clientService.updateClient(client);

		String message = "Client was successfully edited.";
		modelAndView.addObject("message", message);

		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteClient(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("employee-form");
		clientService.deleteClient(id);
		String message = "Client was successfully deleted.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}
}

package com.sprhib.presentation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sprhib.domain.User;
import com.sprhib.service.UserService;



@Controller
@RequestMapping(value = "/user")
public class UserController {
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView loginPage() {
		ModelAndView modelAndView = new ModelAndView("login-form");
		modelAndView.addObject("user", new User());
		return modelAndView;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView loggingUser(@ModelAttribute User user) {
		ModelAndView modelAndView;
		user = userService.login(user.getUsername(), user.getPassword());
		if(user!=null){
			if (user.isAdministrator()) {
			modelAndView = new ModelAndView("administrator-form");

			String message = "Logged as a administrator";
			modelAndView.addObject("message", message);

		} else {
			modelAndView = new ModelAndView("employee-form");
			
			String message = "Logged as a employee";
			modelAndView.addObject("message", message);

		}
		}
		else{//the user doesn't exist
			modelAndView = new ModelAndView("login-form");
			
			String message = "Invalid credentials!!";
			modelAndView.addObject("message", message);
		}

		
		return modelAndView;

	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView addUserPage() {
		ModelAndView modelAndView = new ModelAndView("add-employee-form");
		modelAndView.addObject("user", new User());
		return modelAndView;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addingUser(@ModelAttribute User user) {

		ModelAndView modelAndView = new ModelAndView("administrator-form");
		userService.addUser(user);

		String message = "An employee was successfully added.";
		modelAndView.addObject("message", message);

		return modelAndView;
	}

	@RequestMapping(value = "/list")
	public ModelAndView listOfUsers() {
		ModelAndView modelAndView = new ModelAndView("list-of-employees");

		List<User> users = userService.getUsers();
		modelAndView.addObject("users", users);

		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editUserPage(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("edit-employee-form");
		User user = userService.getUser(id);
		modelAndView.addObject("user", user);
		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public ModelAndView edditingUser(@ModelAttribute User user, @PathVariable Integer id) {

		ModelAndView modelAndView = new ModelAndView("administrator-form");

		userService.updateUser(user);

		String message = "Employee was successfully edited.";
		modelAndView.addObject("message", message);

		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteUser(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("administrator-form");
		userService.deleteUser(id);
		String message = "Employee was successfully deleted.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}

}


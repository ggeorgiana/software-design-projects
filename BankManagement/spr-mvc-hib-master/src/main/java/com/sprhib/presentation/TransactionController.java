package com.sprhib.presentation;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sprhib.service.AccountService;
import com.sprhib.service.TransactionService;
import com.sprhib.domain.Account;
import com.sprhib.domain.Transaction;
import com.sprhib.domain.TransactionDTO;

@Controller
@RequestMapping(value = "/transaction")
public class TransactionController {
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private TransactionService transactionService;
	
	private Integer clientId;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ModelAndView addTransactionPage(@PathVariable Integer id) {
		clientId = id;
		System.out.println("client id:" + id);
		ModelAndView modelAndView = new ModelAndView("add-transaction-form");
		modelAndView.addObject("transaction", new Transaction(id,0,0,null,null,null,0));
		return modelAndView;
	}

	@RequestMapping(value = "/addTransaction/{id}", method = RequestMethod.POST)
	public ModelAndView addingTransaction(@ModelAttribute TransactionDTO transactionDTO, @PathVariable Integer id) {
		System.out.println("client id:" + clientId);
		System.out.println(transactionDTO.getAccountId1());
		System.out.println(transactionDTO.getAccountId2());
		System.out.println(transactionDTO.getTransactionDate());
		System.out.println(transactionDTO.getType());
		System.out.println(transactionDTO.getAmount());
				
		ModelAndView modelAndView = new ModelAndView("employee-form");
		Account account1 = accountService.getAccount(transactionDTO.getAccountId1());//contul sursa la care se adauga lista de tranzactii
		Account account2 = accountService.getAccount(transactionDTO.getAccountId2());
		Double amount = Double.parseDouble(transactionDTO.getAmount());
		
		DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
		Date transactionDate = null;
		try {
			transactionDate = formatter.parse(transactionDTO.getTransactionDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(transactionDate);
		Transaction transaction;
		if (transactionDTO.getType().equals("Transfer")){
			transaction = new Transaction(clientId,transactionDTO.getAccountId1(),transactionDTO.getAccountId2(),account1, transactionDate, Transaction.Type.TRANSFER,amount );
			account1.setBalance(account1.getBalance() - amount);
			account2.setBalance(account2.getBalance() + amount);
		}
		else if (transactionDTO.getType().equals("Pay Bill")){   //pay bills
			transaction = new Transaction(clientId,transactionDTO.getAccountId1(),transactionDTO.getAccountId2(),account1, transactionDate, Transaction.Type.PAYBILL, amount);
			account1.setBalance(account1.getBalance() - amount);
		}
		else if (transactionDTO.getType().equals("Withdraw")){   
			transaction = new Transaction(clientId,transactionDTO.getAccountId1(),transactionDTO.getAccountId2(),account1, transactionDate, Transaction.Type.WITHDRAW, amount);
			account1.setBalance(account1.getBalance() - amount);
		}
		else{ //deposit   
			transaction = new Transaction(clientId,transactionDTO.getAccountId1(),transactionDTO.getAccountId2(),account1, transactionDate, Transaction.Type.DEPOSIT, amount);
			account1.setBalance(account1.getBalance() + amount);
		}
		transactionService.addTransaction(transaction);
		accountService.updateAccount(account1);
		accountService.updateAccount(account2);
	//	accountService.getAccountTransactions(account1.getId()).add(transaction);

		String message = "An transaction was successfully performed.";
		modelAndView.addObject("message", message);

		return modelAndView;
	}
	@RequestMapping(value = "/list")
	public ModelAndView listOfTransactions() {
		ModelAndView modelAndView = new ModelAndView("list-of-transactions");

		List<Transaction> transactions = transactionService.getTransactions();
		modelAndView.addObject("transactions", transactions);

		return modelAndView;
	}
}


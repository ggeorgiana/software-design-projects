package com.sprhib.presentation;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sprhib.service.AccountService;
import com.sprhib.service.ClientService;
import com.sprhib.domain.Account;
import com.sprhib.domain.AccountDTO;
import com.sprhib.domain.Client;

@Controller
@RequestMapping(value = "/account")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private ClientService clientService;
	
	private Integer clientId;

	@RequestMapping(value = "/add/{id}", method = RequestMethod.GET)
	public ModelAndView addAccountPage(@PathVariable Integer id) {
		clientId = id;
		System.out.println("client id:" + id);
		ModelAndView modelAndView = new ModelAndView("add-account-form");
		modelAndView.addObject("account", new Account(clientService.getClient(id), null, null, 0));
		return modelAndView;
	}

	@RequestMapping(value = "/addAccount/{id}", method = RequestMethod.POST)
	public ModelAndView addingAccount(@ModelAttribute AccountDTO accountDto, @PathVariable Integer id) {
		System.out.println("client id:" + clientId);
		System.out.println(accountDto.getBalance());
		System.out.println(accountDto.getCreationDate());
		System.out.println(accountDto.getType());

		ModelAndView modelAndView = new ModelAndView("employee-form");
		Client client = clientService.getClient(clientId);

		DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
		Date creationDate = null;
		try {
			creationDate = formatter.parse(accountDto.getCreationDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(creationDate);
		Account acc;
		if (accountDto.getType().equals("Spending"))
			acc = new Account(client, Account.Type.SPENDING, creationDate, Double.parseDouble(accountDto.getBalance()));
		else
			acc = new Account(client, Account.Type.SAVING, creationDate, Double.parseDouble(accountDto.getBalance()));
		accountService.addAccount(acc);
		clientService.getClientAccounts(clientId).add(acc);

		String message = "An account was successfully added.";
		modelAndView.addObject("message", message);

		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editAccountPage(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("edit-account-form");
		Account account = accountService.getAccount(id);

		modelAndView.addObject("account", account);
		clientId = account.getClient().getId();
	//	editCreationDate = account.getCreationDate();
		return modelAndView;
	}

	@RequestMapping(value = "/editAccount/{id}", method = RequestMethod.POST)
	public ModelAndView edditingAccount(@ModelAttribute AccountDTO accountDto, @PathVariable Integer id) {
		System.out.println("acc id:" + id);
		System.out.println(accountDto.getBalance());
		System.out.println(accountDto.getCreationDate());
		System.out.println(accountDto.getType());

		Client client = clientService.getClient(clientId);

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date creationDate = null;
		String myDate;
		if (accountDto.getCreationDate().indexOf(' ') > 0)
			myDate = accountDto.getCreationDate().substring(0, accountDto.getCreationDate().indexOf(' ')).trim();
		else
			myDate = accountDto.getCreationDate();
		System.out.println(myDate);

		try {
			creationDate = formatter.parse(myDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(creationDate);

		Account acc;
		if (accountDto.getType().equals("Spending"))
			acc = new Account(client, Account.Type.SPENDING, creationDate, Double.parseDouble(accountDto.getBalance()));
		else
			acc = new Account(client, Account.Type.SAVING, creationDate, Double.parseDouble(accountDto.getBalance()));
		acc.setId(id);
		accountService.updateAccount(acc);

		ModelAndView modelAndView = new ModelAndView("list-of-clients");

		String message = "Account was successfully edited.";
		modelAndView.addObject("message", message);

		return modelAndView;
	}

	@RequestMapping(value = "/list")
	public ModelAndView listOfAccounts() {
		ModelAndView modelAndView = new ModelAndView("list-of-accounts");

		List<Account> accounts = accountService.getAccounts();
		modelAndView.addObject("accounts", accounts);

		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteAccount(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("list-of-clients");
		accountService.deleteAccount(id);
		String message = "Account was successfully deleted.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}

}

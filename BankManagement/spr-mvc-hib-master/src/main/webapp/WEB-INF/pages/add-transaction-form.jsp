<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Add transaction page</title>
</head>
<body>
	<h1>Add transaction page</h1>
	<p>${transaction.id}</p>
	<p>Here you can add a new transaction.</p>
	<form:form method="POST" commandName="transaction"
		action="${pageContext.request.contextPath}/transaction/addTransaction/${transaction.idClient}" >
		<table>
			<tbody>		
			 	<tr> 
 					<td>Source Account ID:</td> 
					<td><form:input path="accountId1" /></td>
 				</tr> 
 					<tr> 
 					<td>Destination Account ID:</td> 
					<td><form:input path="accountId2" /></td>
				</tr>  
					<tr>
					<td>Transaction Type:</td>
					<td>
						<select name="type">
						    <option value="Transfer">Transfer</option>
						    <option value="PayBill">Pay Bill</option>
						    <option value="Withdraw">Withdraw</option>
						    <option value="Deposit">Deposit</option>
					    </select>					
 					</td>
				</tr>
				<tr>
					<td>Transaction Date:</td>
					<td><form:input path="transactionDate" /></td>
				</tr>
				<tr>
					<td>Amount:</td>
					<td><form:input path="amount" /></td>
				</tr>
				<tr>
					<td><input type="submit" value="Execute Transaction" /></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</form:form>

	<p>
		<a href="${pageContext.request.contextPath}/account/list.html">Accounts list page</a>
	</p>
</body>
</html>
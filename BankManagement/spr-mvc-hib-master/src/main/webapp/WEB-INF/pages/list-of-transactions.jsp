<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>List of transactions</title>
</head>
<body>
	<h1>List of transactions</h1>
	<p>Here you can see the list of the transactions</p>
	<table border="1px" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th width="10%">Transaction ID</th>
				<th width="10%">Client ID</th>
				<th width="15%">Source Account ID</th>
				<th width="10%">Destination Account ID</th>
				<th width="15%">Date of Transaction</th>
				<th width="10%">Type</th>
				<th width="10%">Amount</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="transaction" items="${transactions}">
				<tr>
					<td>${transaction.id}</td>
					<td>${transaction.idClient}</td>
					<td>${transaction.accountId1}</td>
					<td>${transaction.accountId2}</td>
					<td>${transaction.transactionDate}</td>
					<td>${transaction.type}</td>
                	<td>${transaction.amount}</td>		
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<p><a href="${pageContext.request.contextPath}/account/list.html">View list of accounts</a></p>
</body>
</html>
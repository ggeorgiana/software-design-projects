<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>List of clients</title>
</head>
<body>
	<h1>List of clients</h1>
	<p>Here you can see the list of the clients, edit them, remove or
		update.</p>
	<table border="1px" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th width="10%">Id</th>
				<th width="10%">Name</th>
				<th width="15%">ID Card No</th>
				<th width="10%">Personal Code</th>
				<th width="15%">Address</th>
				<th width="10%">Actions</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="client" items="${clients}">
				<tr>
					<td>${client.id}</td>
					<td>${client.name}</td>
					<td>${client.idCardNo}</td>
					<td>${client.personalCode}</td>
					<td>${client.address}</td>
					<td><a href="${pageContext.request.contextPath}/client/edit/${client.id}.html">Edit</a><br />
						<a href="${pageContext.request.contextPath}/client/delete/${client.id}.html">Delete</a><br />				
						<a href="${pageContext.request.contextPath}/account/add/${client.id}.html">Add a new account </a><br />
						<a href="${pageContext.request.contextPath}/transaction/${client.id}.html">Perform Transaction </a><br />
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<p><a href="${pageContext.request.contextPath}/account/list.html">View list of accounts</a><br />
	<a href="${pageContext.request.contextPath}/transaction/list.html">View list of transactions</a></p>


</body>
</html>
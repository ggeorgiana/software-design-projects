<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Edit client page</title>
</head>
<body>
	<h1>Edit client page</h1>
	<p>Here you can edit the existing client.</p>
	<p>${message}</p>
	<form:form method="POST" commandName="client"
		action="${pageContext.request.contextPath}/client/edit/${client.id}.html">
		<table>
			<tbody>
				<tr>
					<td>Name:</td>
					<td><form:input path="name" /></td>
				</tr>
				<tr>
					<td>ID Card Number:</td>
					<td><form:input path="idCardNo" /></td>
				</tr>
				<tr>
					<td>Personal Code:</td>
					<td><form:input path="personalCode" /></td>
				</tr>
				<tr>
					<td>Address:</td>
					<td><form:input path="address" /></td>
				</tr>
				<tr>
					<td><input type="submit" value="Edit" /></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</form:form>

	<p>
		<a href="${pageContext.request.contextPath}/client/list.html">List of clients</a>
	</p>
</body>
</html>
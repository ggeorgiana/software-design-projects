<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Add account page</title>
</head>
<body>
	<h1>Add account page</h1>
	<p>${account.client.id}</p>
	<p>Here you can add a new account.</p>
	<form:form method="POST" commandName="account"
		action="${pageContext.request.contextPath}/account/addAccount/${account.client.id}" >
		<table>
			<tbody>		
				<tr>
					<td>Account Type:</td>
					<td>
					<select name="type">
						    <option value="Spending">Spending</option>
						    <option value="Saving">Saving</option>
				 </select></td>

				</tr>
				<tr>
					<td>Creation Date:</td>
					<td><form:input path="creationDate" /></td>
				</tr>
				<tr>
					<td>Balance:</td>
					<td><form:input path="balance" /></td>
				</tr>
				<tr>
					<td><input type="submit" value="Add" /></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</form:form>

	<p>
		<a href="${pageContext.request.contextPath}/account/list.html">Accounts list page</a>
	</p>
</body>
</html>